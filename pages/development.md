## ESPAÑOL

Si has podido mejorar algo haz un [Pull Request](https://guides.github.com/activities/forking/#making-a-pull-request) 

Si has hecho, o quieres hacer, una version haz un [derivación](https://guides.github.com/activities/forking/#fork).

Por favor, cuentanos tus mejoras/ideas. https://t.me/librevisor Intentaremos difundirlo. Muchas gracias!

## ENGLISH 

If you improved something, please fo a [Pull Request](https://guides.github.com/activities/forking/#making-a-pull-request) 

If you forked or want to fork this version, please do a [fork](https://guides.github.com/activities/forking/#fork).

Please, tell us about your changes/ideas. https://t.me/librevisor We will try to spread them. Muchas gracias!

---

LICENSE GPLv3 - this is a libre product for a libre world

https://www.gnu.org/licenses/gpl-3.0.html


---

[Otros proyectos / Other projects](https://publishwith.me/QvTsJTppfb)
