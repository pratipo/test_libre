## ESPAÑOL

Cuidado! Este producto es de plástico.  
Intentad reutilizarlo tanto como podáis, y si nos es posible destinadlo a reciclaje.
:)


## ENGLISH

Be careful! This product is made of plastic.  
Try to reuse it as much as you can, and if not possible then please recycle it.
:)
