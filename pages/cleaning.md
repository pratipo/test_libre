## ESPAÑOL

Desinfectar con alcohol etílico.

Autoclave:

| Material | Apto |
| ------ | ------ |
| PET | NO |
| PVC | NO |
| PP (Plakene, Polipropileno) | SÍ |

Para otros métodos de esterilización, consultar compatibilidad en esta tabla: https://www.industrialspec.com/resources/plastics-sterilization-compatibility.





## ENGLISH

Disinfect with ethyl alcohol.

Autoclave:

| Material | Apt |
| ------ | ------ |
| PET | NO |
| PVC | NO |
| PP (Plakene, Polypropylene) | YES | 

For other sterilization methods, consult the following table: https://www.industrialspec.com/resources/plastics-sterilization-compatibility.