# LIBREGUARD
---
Libreguard es una **máscara DIY libre** para frenar el avance del **COVID-19**.
Encuentra una máquina de corte láser o troqueladora, lámina de plástico y haz 1000 máscaras al día!

Libreguard is a **libre DIY browguard** designed to stop the **COVID-19** spread.
Find a lasercut or die cutting machine, plastic sheet and produce 1000 masks a day!

![alt text](pictures/Resumen.png "matt")

### + info ?
telegram: https://t.me/librevisor

whatsapp: https://chat.whatsapp.com/HU48aiCKV854d3TMxsNhhw

---

## ESPAÑOL

* Producción [rápida](pages/lasercut.md) (1 minuto) y [barata](pages/cost.md) (0.20€) de una hoja de un solo material.
* En [materiales](pages/materiales.md) sanitarios baratos.
* Fácil y rápido [montaje](pages/assembly.md) : 1 minuto.
* Fácil y rápida [limpieza](pages/cleaning.md), frontal desechable, estructura reultilizable.

* [Ecosistema!](pages/ecosystem.md)
* [Mejorásteis el diseño?](pages/development.md) :)

---

## ENGLISH

* Its designed to be produced [fast](pages/lasercut.md) (1 minute) and [cheap](pages/cost.md) (0.20 USD) and from a single material sheet.
* In sanitary-compatible [cheap materials](pages/materiales.md).
* Easy and fast to [assemble](pages/assembly.md): 1 minute.
* Easy and fast to [clean](pages/cleaning.md), disposable sheet, reusable structure.

* [Ecosystem!](pages/ecosystem.md)
* [Did you improve the design?](pages/development.md) :)

---

LICENSE GPLv3 - this is a libre product for a libre world

https://www.gnu.org/licenses/gpl-3.0.html

---

![alt text](https://gitlab.com/escola-massana/fed/libreguard/-/raw/76c4160cd06d7d478af2b4f0eb414eca5e553ac9/pictures/libreguard_product.jpg "matt")



